# letompouce/md2html

Generate HTML from various formats.

* Debian Buster
* [`pandoc(1)`](https://pandoc.org/)
* [`tidy(1)`](http://www.html-tidy.org/)

The `md2html` name might change in the future, since pandoc is not limited to
Markdown - neither are you.

## Usage

The `md2html.sh` script is a wrapper for this command:

```shell
docker run \
    --rm \
    --name md2html \
    --volume "$( pwd ):/data" \
    --user $( id -u ):$( id -g ) \
    letompouce/md2html:buster-slim \
    tidy --version
```

Example:

```shell
./md2html.sh pandoc -f markdown -t html README.md > index.html
./md2html.sh -i -u -q -m  index.html
```


